CMAKE_MINIMUM_REQUIRED(VERSION 3.0.2)
set(WORKSPACE_DIR ${CMAKE_SOURCE_DIR}/../.. CACHE PATH "root of the PID workspace directory")
list(APPEND CMAKE_MODULE_PATH ${WORKSPACE_DIR}/share/cmake/system) # using generic scripts/modules of the workspace
include(Package_Definition NO_POLICY_SCOPE)

project(rkcl-panda-robot)

PID_Package(
	AUTHOR      	Benjamin Navarro
	INSTITUTION 	LIRMM / CNRS
	YEAR        	2019
	LICENSE     	CeCILL
	ADDRESS			git@gite.lirmm.fr:rkcl/rkcl-panda-robot.git
	PUBLIC_ADDRESS 	https://gite.lirmm.fr/rkcl/rkcl-panda-robot.git
	DESCRIPTION 	Helper package to work with the Franka Panda robot
	CODE_STYLE		rkcl
	VERSION     	1.1.2
)

PID_Category(robot)
PID_Publishing(PROJECT            https://gite.lirmm.fr/rkcl/rkcl-panda-robot
				FRAMEWORK         rkcl-framework
				DESCRIPTION       Helper package to work with the Franka Panda robot
				ALLOWED_PLATFORMS x86_64_linux_abi11)

PID_Dependency(rkcl-core VERSION 1.1.0)
PID_Dependency(rkcl-driver-panda VERSION 1.1.0)
PID_Dependency(rkcl-fk-rbdyn VERSION 1.1.0)
PID_Dependency(rkcl-otg-reflexxes VERSION 1.1.0)
PID_Dependency(rkcl-collision-sch VERSION 1.1.0)
PID_Dependency(rkcl-osqp-solver VERSION 1.1.2)

if(BUILD_EXAMPLES)
	PID_Dependency(rkcl-app-utility VERSION 1.1.0)
	PID_Dependency(rkcl-driver-vrep VERSION 1.1.0)
	PID_Dependency(rkcl-filters VERSION 1.1.0)
	PID_Dependency(pid-os-utilities VERSION 2.1.1)
endif()

build_PID_Package()
