/**
 * @file main.cpp
 * @author Benjamin Navarro
 * @brief Simple example showing how to control the panda robot on the physical setup
 * @date 18-03-2020
 * License: CeCILL
 */

#include <rkcl/robots/panda.h>
#include <rkcl/processors/task_space_otg.h>
#include <rkcl/processors/app_utility.h>

#include <pid/signal_manager.h>

int main()
{
    rkcl::DriverFactory::add<rkcl::PandaDriver>("panda");
    rkcl::QPSolverFactory::add<rkcl::OSQPSolver>("osqp");

    auto conf = YAML::LoadFile(PID_PATH("panda_config/panda_real.yaml"));
    auto app = rkcl::AppUtility::create<rkcl::ForwardKinematicsRBDyn>(conf);

    rkcl::TaskSpaceOTGReflexxes task_space_otg(
        app.getRobot(), app.getTaskSpaceController().getControlTimeStep());

    auto tcp = app.getRobot().getControlPointByName("tcp");

    if (not app.init())
    {
        throw std::runtime_error("Cannot initialize the application");
    }

    app.addDefaultLogging();

    bool stop = false;
    bool done = false;

    pid::SignalManager::registerCallback(pid::SignalManager::Interrupt, "stop",
                                         [&](int) { stop = true; });

    try
    {
        std::cout << "Starting joint control \n";
        app.configureTask(0);
        while (not stop and not done)
        {
            done = not app.runJointSpaceLoop();

            if (not done)
            {
                done = true;
                for (auto joint_controller : app.getJointControllers())
                    if (joint_controller)
                        done &=
                            (joint_controller->getErrorGoalNorm() < 0.00001);
            }
        }

        app.nextTask();
        done = false;
        task_space_otg.reset();

        std::cout << "First task loop (OTG)" << std::endl;
        while (not stop and not done)
        {
            done = not app.runTaskSpaceLoop(
                [&task_space_otg] { return task_space_otg(); });

            if (not done)
            {
                done = not(app.getTaskSpaceController()
                               .getControlPointsPoseErrorGoalNormPosition() > 0.01);
                done &= (task_space_otg.getResult() ==
                         rml::ResultValue::FinalStateReached);
            }

            if (done)
            {
                std::cout << "Task completed, moving to the next one"
                          << std::endl;
                app.nextTask();
                task_space_otg.reset();
            }
        }
        if (stop)
        {
            throw std::runtime_error("Caught user interruption, aborting");
        }

        std::cout << "Second task loop (Damping)" << std::endl;
        while (not stop)
        {
            stop |= not app.runTaskSpaceLoop();
        }

        std::cout << "All tasks completed" << std::endl;
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    app.getRobot().getJointGroupByName("panda")->command.velocity.setZero();
    pid::SignalManager::unregisterCallback(pid::SignalManager::Interrupt,
                                           "stop");

    std::this_thread::sleep_for(std::chrono::seconds(1));

    app.end();
}