/**
 * @file panda.h
 * @author Benjamin Navarro
 * @brief Header file to include all dependency to the Panda robot
 * @date 18-03-2020
 * License: CeCILL
 */

#pragma once

#include <rkcl/core.h>
#include <rkcl/utils.h>
#include <rkcl/drivers/panda_driver.h>
#include <rkcl/processors/forward_kinematics_rbdyn.h>
#include <rkcl/processors/collision_avoidance_sch.h>
#include <rkcl/processors/osqp_solver.h>
#include <rkcl/processors/otg_reflexxes.h>
